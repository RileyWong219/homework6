/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javax.imageio.ImageIO;
import pm.data.VBoxPrime;
import pm.data.DataManager;
import pm.gui.Workspace;
import saf.AppTemplate;

import static saf.components.AppStyleArbiter.VBOX_BORDER1;
import saf.components.AppWorkspaceComponent;
import saf.controller.AppFileController;
import saf.controller.ControllerComponent;
import saf.ui.AppGUI;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author YungReezy
 */
public class Control implements ControllerComponent{
    AppTemplate app;
    DataManager data;
    
    public Control(AppTemplate app){
        this.app = app;
        data = (DataManager) app.getDataComponent();
    }

    /*
    Creates new handles request
    */
    @Override
    public void handleNewClassRequest() {
       
        
       VBox biggieSmalls = new VBox();
       //biggieSmalls.setMaxHeight(200);
       //biggieSmalls.setMaxWidth(200);
       
       Workspace test = (Workspace) app.getWorkspaceComponent();
       VBoxPrime newV = new VBoxPrime("Class","dummy",data);
       
       VBox vars = new VBox();
       VBox meths = new VBox();
       
       //newV.getChildren().add(new Label("ClassName"));
       newV.getStyleClass().add(VBOX_BORDER1);
       
       
       //test.getStack().getChildren().add(newV);
       
       data.getArray().add(newV);
      // data.getBoxes().add(newV);
       
       
      /* vars.setMinSize(200, 75);
       vars.setMaxWidth(200);
       meths.setMinSize(200, 75);
       meths.setMaxWidth(200);
       */
      //test.getStack().getChildren().add(vars);
      
       data.setSelected(newV, vars, meths,biggieSmalls);
       
       biggieSmalls.setOnMousePressed(e ->{
           data.setSelected( newV,vars,meths,biggieSmalls);
           AppGUI test123 =app.getGUI();
           AppFileController appFil =  new AppFileController(app);
           appFil.markAsEdited(test123);
       });
       
       
       
       biggieSmalls.setOnMouseDragged(e -> {
           if(data.getSelect()){
           if(data.getSnap()==false){
           biggieSmalls.setTranslateX(biggieSmalls.getTranslateX() + e.getX());
           biggieSmalls.setTranslateY(biggieSmalls.getTranslateY() + e.getY());
           VBoxPrime asd = (VBoxPrime)biggieSmalls.getChildren().get(0);
           asd.setXCord(biggieSmalls.getTranslateX()+e.getX());
           asd.setYCord(biggieSmalls.getTranslateY()+e.getY());
           ArrayList<Connection> connec = test.getCon();
           for(int x  = 0; x < connec.size(); x++){
               connec.get(x).follow();
               //System.out.println("do this");
           }
           }
           else{
               double x12 = (biggieSmalls.getTranslateX() + e.getX())/100;
               
               int x = (int) x12;
               double y12 = (biggieSmalls.getTranslateY() + e.getY())/100;
               int y = (int) y12;
               x = x*100;
               y = y*100;
               biggieSmalls.setTranslateX(x);
               biggieSmalls.setTranslateY(y);
               VBoxPrime asd = (VBoxPrime)biggieSmalls.getChildren().get(0);
           asd.setXCord(x);
           asd.setYCord(y);
           ArrayList<Connection> connec = test.getCon();
           for(int c  = 0; c < connec.size(); c++){
               connec.get(c).follow();
               //System.out.println("do this");
           }
           }
           
           AppGUI test123 =app.getGUI();
           AppFileController appFil =  new AppFileController(app);
           appFil.markAsEdited(test123);
           }
           if(data.getResize()){
               double xC = vars.getWidth();            
               double yC = vars.getHeight();
               //System.out.println(biggieSmalls.getTranslateX());
               double diffx =  e.getX();
               //System.out.println(diffx);
               double diffy = e.getY();
               
               xC = diffx; 
               yC = diffy;
               newV.setPrefHeight(yC);
               newV.setPrefWidth(xC);
               vars.setPrefHeight(yC-50-(yC/2.5));
               vars.setPrefWidth(xC);
               meths.setPrefHeight(yC-50-(yC/2.5));
               meths.setPrefWidth(xC);
               //meths.setPrefHeight(yC);
               //meths.setPrefWidth(xC);
               //biggieSmalls.setPrefHeight(yC);
               //biggieSmalls.setPrefWidth(xC);
           }
       });  
       newV.setMaxHeight(50);
       //newV.setMaxHeight(50);
       
       
       biggieSmalls.getChildren().add(newV);
       biggieSmalls.getChildren().add(vars);
       vars.setStyle(newV.getStyle());
       biggieSmalls.getChildren().add(meths);
       meths.setStyle(newV.getStyle());
       //biggieSmalls.getChildren().add(new Label("Testing"));
       test.getStack().getChildren().add(biggieSmalls);
       
    }
    
    
    /*
    Creates new VBox if new interface is clicked
    */
    public void handleNewInterfaceRequest() {
       
       VBox biggieSmalls = new VBox();
       //biggieSmalls.setMaxHeight(200);
       //biggieSmalls.setMaxWidth(200);
       
       Workspace test = (Workspace) app.getWorkspaceComponent();
       VBoxPrime newV = new VBoxPrime("Interface","dummy",data);
       
       VBox vars = new VBox();
       VBox meths = new VBox();
       
       //newV.getChildren().add(new Label("ClassName"));
       newV.getStyleClass().add(VBOX_BORDER1);
       
       
       //test.getStack().getChildren().add(newV);
       
       data.getArray().add(newV);
      // data.getBoxes().add(newV);
       
       
       //vars.setMinHeight(75);
       //vars.setMaxWidth(200);
       //meths.setMinHeight(75);
       //meths.setMaxWidth(200);
       
      //test.getStack().getChildren().add(vars);
      
       data.setSelected(newV, vars, meths,biggieSmalls);
       
       biggieSmalls.setOnMousePressed(e ->{
           data.setSelected( newV,vars,meths,biggieSmalls);
           AppGUI test123 =app.getGUI();
           AppFileController appFil =  new AppFileController(app);
           appFil.markAsEdited(test123);
           ArrayList<Connection> connec = test.getCon();
           for(int x  = 0; x < connec.size(); x++){
               connec.get(x).follow();
               //System.out.println("do this");
           }
       });
       
       
       
       biggieSmalls.setOnMouseDragged(e -> {
           if(data.getSelect()){
           if(data.getSnap()==false){
           biggieSmalls.setTranslateX(biggieSmalls.getTranslateX() + e.getX());
           biggieSmalls.setTranslateY(biggieSmalls.getTranslateY() + e.getY());
           VBoxPrime asd = (VBoxPrime)biggieSmalls.getChildren().get(0);
           asd.setXCord(biggieSmalls.getTranslateX()+e.getX());
           asd.setYCord(biggieSmalls.getTranslateY()+e.getY());
           }
           else{
               double x12 = (biggieSmalls.getTranslateX() + e.getX())/100;
               
               int x = (int) x12;
               double y12 = (biggieSmalls.getTranslateY() + e.getY())/100;
               int y = (int) y12;
               x = x*100;
               y = y*100;
               biggieSmalls.setTranslateX(x);
               biggieSmalls.setTranslateY(y);
               VBoxPrime asd = (VBoxPrime)biggieSmalls.getChildren().get(0);
           asd.setXCord(x);
           asd.setYCord(y);
           ArrayList<Connection> connec = test.getCon();
           for(int c  = 0; c < connec.size(); c++){
               connec.get(c).follow();
               //System.out.println("do this");
           }
           }
           }
           if(data.getResize()){
               double xC = vars.getWidth();            
               double yC = vars.getHeight();
               //System.out.println(biggieSmalls.getTranslateX());
               double diffx =  e.getX();
               //System.out.println(diffx);
               double diffy = e.getY();
               
               xC = diffx; 
               yC = diffy;
               newV.setPrefHeight(yC);
               newV.setPrefWidth(xC);
               vars.setPrefHeight(yC-50-(yC/2.5));
               vars.setPrefWidth(xC);
               meths.setPrefHeight(yC-50-(yC/2.5));
               meths.setPrefWidth(xC);
               //meths.setPrefHeight(yC);
               //meths.setPrefWidth(xC);
               //biggieSmalls.setPrefHeight(yC);
               //biggieSmalls.setPrefWidth(xC);
           }
       });
       newV.setMaxHeight(50);
       //newV.setMaxHeight(50);
       //newV.setMinSize(25, 25);
       
       biggieSmalls.getChildren().add(newV);
       biggieSmalls.getChildren().add(vars);
       vars.setStyle(newV.getStyle());
       biggieSmalls.getChildren().add(meths);
       meths.setStyle(newV.getStyle());
       //biggieSmalls.getChildren().add(new Label("Testing"));
       test.getStack().getChildren().add(biggieSmalls);
       
    }
      
    public void add(VBoxPrime a, String label){
        a.getChildren().add(new Label(label));
    }

    @Override
    public void handleDeleteClassRequest() {
        ArrayList<VBoxPrime> boxes = data.getArray();
        System.out.println("here");
        if(data.getBig()== null){
            data.setBig((VBox)data.getSelected().getParent());
            
        }
        
        if(data.getBig()!= null && data.getSelected()!= null){
            
            Workspace test = (Workspace) app.getWorkspaceComponent();
            test.getStack().getChildren().remove(data.getBig());
            boxes.remove(data.getSelected());
            
            
        }
    }
    
    public void handleSnapShotRequest() {
        
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getStack();
        
	WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
	File file = new File("Picture.png");
	try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            AppMessageDialogSingleton mess = AppMessageDialogSingleton.getSingleton();
            mess.show("Picture","Picture Taken");
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
            AppMessageDialogSingleton mess = AppMessageDialogSingleton.getSingleton();
            mess.show("Picture","Not Taken, There's an error");
	}
        
        
    }

    @Override
    public void test() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

        @Override
        public void handleZoomIn() {
           Workspace workspace = (Workspace)app.getWorkspaceComponent();
            Pane canvas = workspace.getStack();
            final double SCALE_DELTA = 1.1;
            ScrollPane stacks = workspace.getScroll();
            stacks.setHmax(stacks.getHmax()*SCALE_DELTA);
            stacks.setVmax(stacks.getVmax()*SCALE_DELTA);
            canvas.setScaleX(canvas.getScaleX()*SCALE_DELTA);
            canvas.setScaleY(canvas.getScaleY()*SCALE_DELTA);
            //System.out.println("Zoom value" +canvas.getScaleX() +"  "+ canvas.getScaleY());
           
      }

    @Override
    public void handleZoomOut() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
            Pane canvas = workspace.getStack();
            final double SCALE_DELTA = 1.1;
            ScrollPane stacks = workspace.getScroll();
            stacks.setHmax(stacks.getHmax()/SCALE_DELTA);
            stacks.setVmax(stacks.getVmax()/SCALE_DELTA);
            canvas.setScaleX(canvas.getScaleX()/SCALE_DELTA);
            canvas.setScaleY(canvas.getScaleY()/SCALE_DELTA);
            //System.out.println("Zoom value" +canvas.getScaleX() +"  "+ canvas.getScaleY());
    }
    @Override
    public void handleGridRequest(CheckBox a) {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        if(a.isSelected()){
        for(int x = 0; x < 22; x++ ){
            Line orange = new Line();
            orange.setStartX(100*x);
            orange.setStartY(0);
            orange.setEndX(100*x);
            orange.setEndY(2100);
            workspace.getStack().getChildren().add(orange);
            
        }
        for(int y = 0; y < 22; y++ ){
            Line orange = new Line();
            orange.setStartX(0);
            orange.setStartY(100*y);
            orange.setEndX(2100);
            orange.setEndY(y*100);
            workspace.getStack().getChildren().add(orange);
            
        }
        }
        else{
            ArrayList<Line> asdf = new ArrayList<Line>();
            for(int v = 0; v < workspace.getStack().getChildren().size(); v++){
                if(workspace.getStack().getChildren().get(v) instanceof Line){
                asdf.add((Line)workspace.getStack().getChildren().get(v));
                
                }
            }
            workspace.getStack().getChildren().removeAll(asdf);
        }
       
        
        
    }

    @Override
    public void handleSnapRequest(CheckBox a) {
        data.setSnap(a.isSelected());
        
    }

    @Override
    public void handleSelectRequest() {
        data.setSelect(true);
        data.setResize(!data.getSelect());
       // System.out.println("resize : "+data.getResize());
       //System.out.println("select : "+data.getSelect());
    }

    @Override
    public void handleResizeRequest() {
        data.setResize(true);
        data.setSelect(!data.getResize());
        //System.out.println("resize : "+data.getResize());
        //System.out.println("select : "+data.getSelect());
    }
    
    }

