/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import java.io.IOException;
import java.util.ArrayList;
import pm.PoseMaker;
import pm.controller.saveArguments;
import pm.controller.saveMethod;
import pm.controller.saveVariables;
import pm.data.DataManager;
import pm.data.VBoxPrime;
import pm.file.FileManager;

/**
 *
 * @author YungReezy
 */
public class TestSave {
    VBoxPrime threadEx;
    VBoxPrime counterTask;
    VBoxPrime dateTask;
    VBoxPrime pauseHandler;
    VBoxPrime startHandler;

    public VBoxPrime getThreadEx() {
        return threadEx;
    }

    public VBoxPrime getCounterTask() {
        return counterTask;
    }

    public VBoxPrime getDateTask() {
        return dateTask;
    }

    public VBoxPrime getPauseHandler() {
        return pauseHandler;
    }

    public VBoxPrime getStartHandler() {
        return startHandler;
    }
    
    
    
    ArrayList<saveArguments> temp;
    
    public TestSave(DataManager date){
        threadEx = new VBoxPrime("Class","ThreadExample",date);
        //initializeThread();
        counterTask = new VBoxPrime("Class", "CounterTask",date);
        dateTask = new VBoxPrime("Class","DateTask",date);
        pauseHandler = new VBoxPrime("Class", "PauseHandler",date);
        startHandler = new VBoxPrime("Class", "StartHandler",date);
        temp = new ArrayList<saveArguments>();
        initializeThread();
        initCounterTask();
        initDateTask();
        initPauseHandler();
        initStartHandler();
    }
    
    private void initializeThread(){
        temp= new ArrayList<saveArguments>();
        //threadEx.addVar(new saveVariables(Name, "Public", static, final));
        // Add variables to thread example
        threadEx.addVar(new saveVariables("START_TEXT", "Public", true, false,"String"));
        threadEx.addVar(new saveVariables("PAUSE_TEXT", "Public", true, false,"String"));
        threadEx.addVar(new saveVariables("window", "Private", false, false,"Stage"));
        threadEx.addVar(new saveVariables("appPane", "Private", false, false,"BorderPane"));
        threadEx.addVar(new saveVariables("topPane", "Private", false, false,"FlowPane"));
        threadEx.addVar(new saveVariables("startButton", "Private", false, false,"Button"));
        threadEx.addVar(new saveVariables("pauseButton", "Private", false, false,"Button"));
        threadEx.addVar(new saveVariables("scrollPane", "Private", false, false,"ScrollPane"));
        threadEx.addVar(new saveVariables("textArea", "Private", false, false,"TextArea"));
        threadEx.addVar(new saveVariables("dateThread", "Private", false, false,"Thread"));
        threadEx.addVar(new saveVariables("dateTask", "Private", false, false,"Task"));
        threadEx.addVar(new saveVariables("counterThread", "Private", false, false,"Thread"));
        threadEx.addVar(new saveVariables("counterTask", "Private", false, false,"Task"));
        threadEx.addVar(new saveVariables("work", "Private", false, false,"boolean"));
        
        temp.add(new saveArguments("primaryStage","Stage"));
        
        //public saveMethod(String name, String type, String returnType,
        //boolean isFinal, boolean isStatic, boolean isAbstract, ArrayList<saveArguments> a ){
        //System.out.println(temp.get(0).getName());
        saveMethod check = new saveMethod("start","Public","void",false,false,false, temp);
        threadEx.addMeth(check);
        //System.out.println(temp.get(0).getName());
        
        temp = new ArrayList<saveArguments>();
        threadEx.addMeth(new saveMethod("startWork","Public","void",false,false,false, temp));
        threadEx.addMeth(new saveMethod("pauseWork","Public","void",false,false,false, temp));
        threadEx.addMeth(new saveMethod("doWork","Public","boolean",false,false,false, temp));
        //System.out.println(check.getArgs().get(0).getName());
        temp.add(new saveArguments("textToAppend","String"));
        threadEx.addMeth(new saveMethod("appendText","Public","void",false,false,false, temp));
        temp = new ArrayList<saveArguments>();
        
        threadEx.addMeth(new saveMethod("sleep","Public","void",false,false,false, temp));
        
        threadEx.addMeth(new saveMethod("initLayout","Private","void",false,false,false, temp));
        threadEx.addMeth(new saveMethod("initHandlers","Private","void",false,false,false, temp));
        temp.add(new saveArguments("initPrimaryStage", "Stage"));
        threadEx.addMeth(new saveMethod("initWindow","Private","void",false,false,false, temp));
        temp= new ArrayList<saveArguments>();
        threadEx.addMeth(new saveMethod("initThreads","Private","void",false,false,false, temp));
        temp.add(new saveArguments("args","String[]"));
        threadEx.addMeth(new saveMethod("main","Public","void",false,true,false, temp));
        temp= new ArrayList<saveArguments>();
        
    }
    
    private void initCounterTask(){
        //threadEx.addVar(new saveVariables(Name, "Public", static, final));
        // Add variables to thread example
        //threadEx.addVar(new saveVariables("START_TEXT", "Public", true, false,"String"));
        counterTask.addVar(new saveVariables("app","Private",false,false,"ThreadExample"));
        counterTask.addVar(new saveVariables("counter","Private",false,false,"int"));
        temp= new ArrayList<saveArguments>();
        temp.add(new saveArguments("initApp","ThreadExample"));
        counterTask.addMeth(new saveMethod("CounterTask","Public","CounterTask",false,false,false,temp));
        counterTask.addMeth(new saveMethod("call","Protected","void",false, false, false, temp));
        temp= new ArrayList<saveArguments>();
    }
    
    private void initDateTask(){
        dateTask.addVar(new saveVariables("app","Private", false,false, "ThreadExample"));
        dateTask.addVar(new saveVariables("now","Private", false, false, "Date"));
        
        temp= new ArrayList<saveArguments>();
        temp.add(new saveArguments("initApp","ThreadExample"));
        dateTask.addMeth(new saveMethod("DateTask","Public","DateTask",false, false, false, temp));
        temp= new ArrayList<saveArguments>();
        dateTask.addMeth(new saveMethod("call","Protected","void", false, false, false, temp));
        temp= new ArrayList<saveArguments>();
    }
    
    private void initPauseHandler(){
        pauseHandler.addVar(new saveVariables("app","Private", false, false, "ThreadExample"));
        temp= new ArrayList<saveArguments>();
        temp.add(new saveArguments("initApp","ThreadExample"));
        pauseHandler.addMeth(new saveMethod("PauseHandler","Public","PauseHandler",false,false,false,temp));
        temp= new ArrayList<saveArguments>();
        temp.add(new saveArguments("event","Event"));
        pauseHandler.addMeth(new saveMethod("handle","Public","void",false,false,false,temp));
        temp= new ArrayList<saveArguments>();
    }
    
    /*
    public saveVariables(String name , 
    String accessibility, 
    boolean isStatic, boolean isFinal, String type){
       
    */
    private void initStartHandler(){
        saveVariables a = new saveVariables("app","Private", false, false, "ThreadExample");
        startHandler.addVar(a);
        temp= new ArrayList<saveArguments>();
        temp.add(new saveArguments("initApp","ThreadExample"));
        startHandler.addMeth(new saveMethod("StartHandler","Public","StartHandler",false,false,false,temp));
        temp= new ArrayList<saveArguments>();
        temp.add(new saveArguments("event","Event"));
        startHandler.addMeth(new saveMethod("handle","Public","void", false, false, false, temp));
        temp= new ArrayList<saveArguments>();
    }
    
    public static void main(String[] args) {
		PoseMaker tes = new PoseMaker();
		DataManager dataManager = new DataManager(tes);
		FileManager fileManager = new FileManager();
                TestSave dc = new TestSave(dataManager);
                dataManager.getArray().add(dc.getCounterTask());
                dataManager.getArray().add(dc.getDateTask());
                dataManager.getArray().add(dc.getPauseHandler());
                dataManager.getArray().add(dc.getStartHandler());
                dataManager.getArray().add(dc.getThreadEx());
		try {
                    
			fileManager.saveData(dataManager, "work/DesignSaveTest.json");
                        
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
}
