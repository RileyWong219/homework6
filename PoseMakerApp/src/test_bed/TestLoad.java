/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import java.io.IOException;
import java.util.ArrayList;
import pm.PoseMaker;
import pm.data.DataManager;
import pm.data.VBoxPrime;
import pm.file.FileManager;
import saf.AppTemplate;
import saf.controller.AppFileController;

/**
 *
 * @author YungReezy
 */
public class TestLoad {
    public TestLoad(AppTemplate app){
        AppFileController fileController = new AppFileController(app);
        fileController.save();
        DataManager dataManager = (DataManager) app.getDataComponent();
        
        ArrayList<VBoxPrime> boxes = dataManager.getArray();
        
        for(int x =0; x < boxes.size(); x++){
            System.out.println(boxes.get(x).toString());
        }
        
    }
    public TestLoad(DataManager data){
        
        
        ArrayList<VBoxPrime> boxes = data.getArray();
        
        for(int x =0; x < boxes.size(); x++){
            System.out.println(boxes.get(x).toString());
        }
        
    }
    
    public static void main(String[] args) {
		PoseMaker tes = new PoseMaker();
		DataManager dataManager = new DataManager(tes);
		FileManager fileManager = new FileManager();
                
		try {
                    
			fileManager.loadData(dataManager, "work/DesignSaveTest.json");
                        TestLoad apps = new TestLoad(dataManager);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
