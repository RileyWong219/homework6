package pm.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import pm.controller.Connection;
import pm.controller.saveArguments;
import pm.controller.saveMethod;
import pm.controller.saveVariables;
import pm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;
import static saf.components.AppStyleArbiter.VBOX_BORDER;
import static saf.components.AppStyleArbiter.VBOX_BORDER1;
import saf.controller.AppFileController;
import saf.ui.AppGUI;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    ObservableList<VBoxPrime> Arrays;
    VBoxPrime selected;
    VBox varBox;
    VBox methBox;
    VBox big;
    
    ComboBox combo;
    ArrayList<VBoxPrime> boxArray;
    boolean select = true;
    boolean resize = false;
    boolean snap = false;
    public VBox getBig(){
        return big;
        
    }
    public void setBig(VBox a){
        big = a;
    }
    public boolean getResize(){
        return resize;
    }
    public boolean getSelect(){
        return select;
    }
    public void setSelect(boolean a){
        select = a;
    }
    public void setResize(boolean a){
        resize = a;
    }
    public boolean getSnap(){
        return snap;
    }
    public void setSnap(boolean a){
        snap = a;
    }
    public VBox getVar(){
        return varBox;
    }
    public VBox getMeth(){
        return methBox;
    }
    public ArrayList getArray(){
        return boxArray;
    }

    public VBoxPrime getSelected(){
        return selected;
    }
    
    public ObservableList<VBoxPrime> getBoxes(){
        return Arrays;
    }
    
    public AppTemplate getApp(){
        return app;
    }
    
    public void setSelected(VBoxPrime a, VBox var, VBox meth ,VBox big1){
        Workspace test = (Workspace) app.getWorkspaceComponent();
        
        
        if(selected!= null){
            
            selected.getStyleClass().clear();
            selected.getStyleClass().add(VBOX_BORDER1);
            varBox.getStyleClass().clear();
            varBox.getStyleClass().add(VBOX_BORDER1);
            methBox.getStyleClass().clear();
            methBox.getStyleClass().add(VBOX_BORDER1);
        }
        combo = test.getCombo();
        combo.getItems().clear();
        /*
        for(int yel = 0; yel < boxArray.size(); yel++){
            VBoxPrime pear = boxArray.get(yel);
            String par = pear.getClassName();
            if(!par.equalsIgnoreCase("dummy")){
            
            combo.getItems().add(par);
            }
        }*/
        
        
        
        
        selected = a;
         //System.out.println(selected.getParent1());
        
        if(selected.getParent1()!=null){
        String str = selected.getParent1();
        String[] parents = str.split(",");
        test.getCombo().getItems().clear();
        for(int x = 0; x < parents.length; x++){
            if(!parents[x].equalsIgnoreCase("dum")){
            test.getCombo().getItems().add(parents[x]);
            test.getCombo().setValue(parents[x]);
            }
        }
        }
        varBox = var;
        methBox = meth;
        big =big1;
        selected.getStyleClass().clear();
        selected.getStyleClass().add(VBOX_BORDER);
        varBox.getStyleClass().clear();
        varBox.getStyleClass().add(selected.getStyleClass().get(0));
        //System.out.println("This is the style " +selected.getStyleClass().get(0));
        methBox.getStyleClass().clear();
        methBox.getStyleClass().add(selected.getStyleClass().get(0));
        /*
        varBox.getStyleClass().clear();
        varBox.getStyleClass().add(selected.getStyleClass().get(0));
        methBox.getStyleClass().clear();
        methBox.getStyleClass().add(selected.getStyleClass().get(0));
        */
        
        test.setText1(((VBoxPrime) selected).getClassName());
        test.setText2(((VBoxPrime) selected).getPackageName());
        
        ComboBox apple = test.getPare();
        
        
        //System.out.println(selected.getStyle());
        
        
        test.update();
        
        
    }
    
    
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp)  {
	// KEEP THE APP FOR LATER
	app = initApp;
        boxArray = new ArrayList();
    }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        
        Workspace test = (Workspace) app.getWorkspaceComponent();
        test.getStack().getChildren().clear();
        if(test!= null){
         for(int p = 0; p < this.getArray().size() ; p++){
            VBoxPrime a = (VBoxPrime) this.getArray().get(p);
            test.getStack().getChildren().add(creation(a)); 
        }
        }
        
        
}public void resetFake() {
        
        Workspace test = (Workspace) app.getWorkspaceComponent();
        test.getStack().getChildren().clear();
        if(test!= null){
         for(int p = 0; p < this.getArray().size() ; p++){
            VBoxPrime a = (VBoxPrime) this.getArray().get(p);
            test.getStack().getChildren().add(creationFake(a)); 
        }
        }
        
        
}
    
    public void reset1(){
        Workspace test = (Workspace) app.getWorkspaceComponent();
        this.getArray().clear();
        test.getStack().getChildren().clear();
    }
    
    
    public VBox creation(VBoxPrime apple){
        VBox biggieSmalls = new VBox();
        //biggieSmalls.setMaxHeight(200);
        //biggieSmalls.setMaxWidth(200);
        //public VBoxPrime(String type, String b, DataManager data)
        VBoxPrime newV = apple;
        //System.out.println(newV.getClassName());
        //System.out.println(newV.getType());
        VBox vars = new VBox();
       VBox meths = new VBox();
       newV.getStyleClass().add(VBOX_BORDER1);
       if(apple.getType().equalsIgnoreCase("interface")){
           if(newV.getChildren().size()<2){
           newV.getChildren().add(new Label("<<interface>>"));
           }
       }
       //vars.setMinHeight(75);
       //vars.setMaxWidth(200);
       //meths.setMinHeight( 75);
       //meths.setMaxWidth(200);
       biggieSmalls.setOnMousePressed(e ->{
           this.setSelected( newV,vars,meths,big);
           AppGUI test123 =app.getGUI();
           AppFileController appFil =  new AppFileController(app);
           appFil.markAsEdited(test123);
           
       });
       
        biggieSmalls.setOnMouseDragged(e -> {
            if(this.getSelect()){
           if(this.getSnap()==false){
           biggieSmalls.setTranslateX(biggieSmalls.getTranslateX() + e.getX());
           biggieSmalls.setTranslateY(biggieSmalls.getTranslateY() + e.getY());
           VBoxPrime asd = (VBoxPrime)biggieSmalls.getChildren().get(0);
           asd.setXCord(biggieSmalls.getTranslateX()+e.getX());
           asd.setYCord(biggieSmalls.getTranslateY()+e.getY());
           Workspace test = (Workspace) app.getWorkspaceComponent();
           ArrayList<Connection> connec = test.getCon();
           for(int x  = 0; x < connec.size(); x++){
               connec.get(x).follow();
               //System.out.println("do this");
           }
           }
           else{
               double x12 = (biggieSmalls.getTranslateX() + e.getX())/100;
               
               int x = (int) x12;
               double y12 = (biggieSmalls.getTranslateY() + e.getY())/100;
               int y = (int) y12;
               x = x*100;
               y = y*100;
               biggieSmalls.setTranslateX(x);
               biggieSmalls.setTranslateY(y);
               VBoxPrime asd = (VBoxPrime)biggieSmalls.getChildren().get(0);
           asd.setXCord(x);
           asd.setYCord(y);
           Workspace test = (Workspace) app.getWorkspaceComponent();
           ArrayList<Connection> connec = test.getCon();
           for(int c  = 0; c < connec.size(); c++){
               connec.get(c).follow();
               //System.out.println("do this");
           }
           }
           AppGUI test123 =app.getGUI();
           AppFileController appFil =  new AppFileController(app);
           appFil.markAsEdited(test123);
            }
            if(this.getResize()){
               double xC = vars.getWidth();            
               double yC = vars.getHeight();
               //System.out.println(biggieSmalls.getTranslateX());
               double diffx =  e.getX();
               //System.out.println(diffx);
               double diffy = e.getY();
               
               xC = diffx; 
               yC = diffy;
               newV.setPrefHeight(yC);
               newV.setPrefWidth(xC);
               vars.setPrefHeight(yC-50-(yC/2.5));
               vars.setPrefWidth(xC);
               meths.setPrefHeight(yC-50-(yC/2.5));
               meths.setPrefWidth(xC);
               //meths.setPrefHeight(yC);
               //meths.setPrefWidth(xC);
               //biggieSmalls.setPrefHeight(yC);
               //biggieSmalls.setPrefWidth(xC);
           }
       });
       newV.setMaxHeight(50);
      // newV.setMaxHeight(50);
       //newV.setMinSize(50, 50);
       
       ArrayList<saveMethod> saveMeth = newV.getMethods();
       ArrayList<saveVariables> saveVars = newV.getVariables();
       ArrayList<saveArguments> args;
       for(int x = 0; x < saveMeth.size();x++){
           String a = "";
           
           saveMethod pear = saveMeth.get(x);
           if(saveMeth.get(x).getType().equalsIgnoreCase("public")){
               a += "+";
           }
           if(saveMeth.get(x).getType().equalsIgnoreCase("private")){
               a += "-";
           }
           a+= pear.getName()+"(";
           ArrayList<saveArguments> args1 = pear.getArgs();
           for(int v = 0; v < args1.size();v++){
               a+= args1.get(v).getType() + " : " + args1.get(v).getName();
               if(v != args1.size()){
                   
               }
                else{
                   a+= " , ";
               }
           }
           a+= ") : " + pear.getReturnType();
           meths.getChildren().add(new Label(a));
       }
       for(int p = 0; p < saveVars.size(); p++){
           String a = "";
           if(saveVars.get(p).getAccessibility().equalsIgnoreCase("public")){
               a += "+";
           }
           if(saveVars.get(p).getAccessibility().equalsIgnoreCase("private")){
               a += "-";
           }
           if(saveVars.get(p).isIsStatic()){
               a+= "$";
           }
          
           a+= saveVars.get(p).getName() + " : ";
           a+= saveVars.get(p).getType();
           vars.getChildren().add(new Label(a));
       }
       biggieSmalls.getChildren().add(newV);
       biggieSmalls.getChildren().add(vars);
       vars.getStyleClass().add(newV.getStyleClass().get(0));
       biggieSmalls.getChildren().add(meths);
       meths.getStyleClass().add(newV.getStyleClass().get(0));
       biggieSmalls.setTranslateX(newV.getTranslateX());
       biggieSmalls.setTranslateY(newV.getTranslateY());
       newV.setTranslateX(0);
       newV.setTranslateY(0);
       
       //System.out.println(newV.getTranslateX() + " NEW V VS BOX " + biggieSmalls.getTranslateX());
        return biggieSmalls;
    }
    
    public VBox creationFake(VBoxPrime apple){
        VBox biggieSmalls = new VBox();
        //biggieSmalls.setMaxHeight(200);
        //biggieSmalls.setMaxWidth(200);
        //public VBoxPrime(String type, String b, DataManager data)
        VBoxPrime newV = apple;
        //System.out.println(newV.getClassName());
        //System.out.println(newV.getType());
        VBox vars = new VBox();
       VBox meths = new VBox();
       newV.getStyleClass().add(VBOX_BORDER1);
       if(apple.getType().equalsIgnoreCase("interface")){
           if(newV.getChildren().size()<2){
           newV.getChildren().add(new Label("<<interface>>"));
           }
       }
       //vars.setMinHeight(75);
       //vars.setMaxWidth(200);
       //meths.setMinHeight( 75);
       //meths.setMaxWidth(200);
       biggieSmalls.setOnMousePressed(e ->{
           this.setSelected( newV,vars,meths,big);
           AppGUI test123 =app.getGUI();
           AppFileController appFil =  new AppFileController(app);
           appFil.markAsEdited(test123);
           
       });
       
        biggieSmalls.setOnMouseDragged(e -> {
            if(this.getSelect()){
           if(this.getSnap()==false){
           biggieSmalls.setTranslateX(biggieSmalls.getTranslateX() + e.getX());
           biggieSmalls.setTranslateY(biggieSmalls.getTranslateY() + e.getY());
           VBoxPrime asd = (VBoxPrime)biggieSmalls.getChildren().get(0);
           asd.setXCord(biggieSmalls.getTranslateX()+e.getX());
           asd.setYCord(biggieSmalls.getTranslateY()+e.getY());
           Workspace test = (Workspace) app.getWorkspaceComponent();
           ArrayList<Connection> connec = test.getCon();
           for(int x  = 0; x < connec.size(); x++){
               connec.get(x).follow();
               //System.out.println("do this");
           }
           }
           else{
               double x12 = (biggieSmalls.getTranslateX() + e.getX())/100;
               
               int x = (int) x12;
               double y12 = (biggieSmalls.getTranslateY() + e.getY())/100;
               int y = (int) y12;
               x = x*100;
               y = y*100;
               biggieSmalls.setTranslateX(x);
               biggieSmalls.setTranslateY(y);
               VBoxPrime asd = (VBoxPrime)biggieSmalls.getChildren().get(0);
           asd.setXCord(x);
           asd.setYCord(y);
           Workspace test = (Workspace) app.getWorkspaceComponent();
           ArrayList<Connection> connec = test.getCon();
           for(int b  = 0; b < connec.size(); b++){
               connec.get(b).follow();
               //System.out.println("do this");
           }
           }
           AppGUI test123 =app.getGUI();
           AppFileController appFil =  new AppFileController(app);
           appFil.markAsEdited(test123);
            }
            if(this.getResize()){
               double xC = vars.getWidth();            
               double yC = vars.getHeight();
               //System.out.println(biggieSmalls.getTranslateX());
               double diffx =  e.getX();
               //System.out.println(diffx);
               double diffy = e.getY();
               
               xC = diffx; 
               yC = diffy;
               newV.setPrefHeight(yC);
               newV.setPrefWidth(xC);
               vars.setPrefHeight(yC-50-(yC/2.5));
               vars.setPrefWidth(xC);
               meths.setPrefHeight(yC-50-(yC/2.5));
               meths.setPrefWidth(xC);
               //meths.setPrefHeight(yC);
               //meths.setPrefWidth(xC);
               //biggieSmalls.setPrefHeight(yC);
               //biggieSmalls.setPrefWidth(xC);
           }
       });
       newV.setMaxHeight(50);
      // newV.setMaxHeight(50);
       //newV.setMinSize(50, 50);
       
       ArrayList<saveMethod> saveMeth = newV.getMethods();
       ArrayList<saveVariables> saveVars = newV.getVariables();
       ArrayList<saveArguments> args;
       for(int x = 0; x < saveMeth.size();x++){
           String a = "";
           
           saveMethod pear = saveMeth.get(x);
           if(saveMeth.get(x).getType().equalsIgnoreCase("public")){
               a += "+";
           }
           if(saveMeth.get(x).getType().equalsIgnoreCase("private")){
               a += "-";
           }
           if(saveMeth.get(x).getStatic()){
               a+= "$";
           }
           a+= pear.getName()+"(";
           ArrayList<saveArguments> args1 = pear.getArgs();
           for(int v = 0; v < args1.size();v++){
               a+= args1.get(v).getType() + " : " + args1.get(v).getName();
               if(v != args1.size()){
                   
               }
                else{
                   a+= " , ";
               }
           }
           a+= ") : " + pear.getReturnType();
           meths.getChildren().add(new Label(a));
       }
       
       for(int p = 0; p < saveVars.size(); p++){
           String a = "";
           if(saveVars.get(p).getAccessibility().equalsIgnoreCase("public")){
               a += "+";
           }
           if(saveVars.get(p).getAccessibility().equalsIgnoreCase("private")){
               a += "-";
           }
           if(saveVars.get(p).isIsStatic()){
               a+= "$";
           }
          
           a+= saveVars.get(p).getName() + " : ";
           a+= saveVars.get(p).getType();
           vars.getChildren().add(new Label(a));
       }
       biggieSmalls.getChildren().add(newV);
       
       biggieSmalls.getChildren().add(vars);
       vars.getStyleClass().clear();
       vars.getStyleClass().add(newV.getStyleClass().get(0));
       
       biggieSmalls.getChildren().add(meths);
       meths.getStyleClass().clear();
       meths.getStyleClass().add(newV.getStyleClass().get(0));
       
       newV.getStyleClass().clear();
       newV.getStyleClass().add(meths.getStyleClass().get(0));
       biggieSmalls.setTranslateX(newV.xCord);
       biggieSmalls.setTranslateY(newV.yCord);
       newV.setTranslateX(0);
       newV.setTranslateY(0);
       
       //System.out.println(newV.xCord + ","+ newV.yCord);
       //System.out.println(newV.getTranslateX() + " NEW V VS BOX " + biggieSmalls.getTranslateX());
        return biggieSmalls;
    }
}
