/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.controller;

import java.util.ArrayList;

/**
 *
 * @author YungReezy
 */
public class saveMethod {
    String name;
    String type;
    String returnType;
    
    boolean isFinal;
    boolean isStatic;
    boolean isAbstract;
    ArrayList<saveArguments> args;

   
    public void setStatic(boolean a){
        isStatic = a;
    }
    public void setAbstract(boolean a){
        isAbstract = a;
    }
    
    public void setFinal(boolean a){
        isFinal = a;
    }
    public boolean getFinal() {
        return isFinal;
    }

    public boolean getStatic() {
        return isStatic;
    }

    public ArrayList<saveArguments> getArgs() {
        return args;
    }
    
    
    
    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getReturnType() {
        return returnType;
    }

    public boolean getAbstract(){
        return isAbstract;
    }

    
    
    
    
    public saveMethod(String name, String type, String returnType, boolean isFinal, boolean isStatic, boolean isAbstract, ArrayList<saveArguments> a ){
        this.name = name;
        this.type = type;
        this.returnType = returnType;
        
        this.isFinal = isFinal;
        this.isStatic = isStatic;
        this.isAbstract = isAbstract;
        args = new ArrayList<saveArguments>();
        this.args = a;
        
    }
    
    
    
    public void setArg(ArrayList<saveArguments> a){
        args = a;
    }
    
}
