/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;

import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import pm.controller.saveArguments;

import pm.controller.saveMethod;
import pm.controller.saveVariables;
import saf.AppTemplate;
import static saf.components.AppStyleArbiter.VBOX_BORDER1;
import saf.controller.AppFileController;

/**
 *
 * @author YungReezy
 */
public class VBoxPrime extends VBox{ 
    String className;
    String packageName;
    String parent="dum";
    String type="";
    
    double xCord=0.0;
    double yCord=0.0;
    
    public void setXCord(double a){
        xCord = a;
    }
    public void setYCord(double a){
        yCord = a;
    }
    public double getXCord(){
        return xCord;
        
    }
    public double getYCord(){
        return yCord;
        
    }
    
    public String toString(){
        String fina = "";
        fina += "Class Name: " +className +"\n";
        fina += "Package Name: " + packageName + "\n";
        fina += "Type: " + type + "\n";
        System.out.print(fina);
        fina += "\n"+ "Variables: \n";
        for(int z = 0; z < variables.size();z++){
            saveVariables appl = variables.get(z);
            
            fina += appl.getName()+ " ";
            fina += appl.getType()+ " \n";
        }
        System.out.print(fina);
        for(int x = 0; x < methods.size();x++){
            saveMethod aas = methods.get(x);
            
            fina += "\n" + "Methods: \n";
            fina += "Name: " + aas.getName() + "\n";
            fina += "Return Type: " + aas.getReturnType() + "\n";
            fina += "Type: "  + aas.getType() + "\n";
            fina += "Abstract: " + aas.getAbstract() + "\n";
            fina += "Static: " + aas.getStatic() + "\n";
            fina += "Final: " + aas.getFinal() + "\n";
            ArrayList<saveArguments> ass = aas.getArgs();
            fina += "\n" + "Arguments: ";
           
            for(int n = 0; n < aas.getArgs().size(); n++){
                fina +=  ass.get(n).getName()+" : ";
                fina += ass.get(n).getType()+"\n";
            }
            System.out.print(fina);
        }
        
        
        
        
        return fina;
    }
    
    public String getParent1() {
        return parent;
    }

    public String getType() {
        return type;
    }
    
    
    ArrayList<saveMethod> methods;
    ArrayList<saveVariables> variables;

    public VBoxPrime() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<saveMethod> getMethods() {
        return methods;
    }

    public ArrayList<saveVariables> getVariables() {
        return variables;
    }

    public VBoxPrime(String type, String b, DataManager data){
        if(type.equals("Interface")){
            this.getChildren().add(new Label("<<Interface>>"));
            this.type = type;
            className = b;
            if(data.getApp().getWorkspaceComponent() != null){
            this.getChildren().add(new Label(className));
            }
        }
        if(type.equals("Class")){
            this.type = type;
            className = b;
            if(data.getApp().getWorkspaceComponent() != null){
            this.getChildren().add(new Label(className));
            }
        }
        //parent = "dummy";
        packageName = "default";
        variables = new ArrayList<saveVariables>();
        methods = new ArrayList<saveMethod>();
        
       // this.app = app;
        
        //data = (DataManager) app.getDataComponent();
        
        this.getStyleClass().add(VBOX_BORDER1);
       /*
        this.setOnMouseDragged(e -> {
           this.setTranslateX(this.getTranslateX() + e.getX());
           this.setTranslateY(this.getTranslateY() + e.getY());
          AppFileController asdf = new AppFileController(data.getApp());
          asdf.markAsEdited(data.getApp().getGUI());
       });
       */
       /*this.setOnMousePressed(e ->{
            data.setSelected(this,data.getVar(),data.getMeth());  
           AppFileController asdf = new AppFileController(data.getApp());
          asdf.markAsEdited(data.getApp().getGUI());
       });*/
       
       this.setMaxWidth(125);
       this.setMaxHeight(50);
       //initVariables();
       //initMethods();
       
    }
    /*
        public void initVariables(){
            varBox = new VBox();
            varBox.setTranslateX(this.getTranslateX());
            varBox.setTranslateY(this.getTranslateY()+this.getHeight());
            varBox.setMaxWidth(125);
            varBox.setMaxHeight(50);
        }
    
        public void initMethods(){
            methBox = new VBox();
        }*/
    
    public void reset(){
        getChildren().clear();
        
        if(type.equals("Interface")){
            getChildren().add(new Label("<<Interface>>"));
            
            getChildren().add(new Label(className));
        }
        if(type.equals("Class")){
            
            this.getChildren().add(new Label(className));
        }
        //packageName = packageName;
    }
    
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    
    public void addMeth(saveMethod a){
        methods.add(a);
    }
    
    public void addVar(saveVariables a){
        variables.add(a);
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public void setType(String type) {
        this.type = type;
    }

    //public String getType() {
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //}
    
    
    
}
