/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.controller;

/**
 *
 * @author YungReezy
 */
public class saveVariables {
    
    String name;
    String accessibility;
    String type;
    boolean isStatic;
    boolean isFinal;
    public void setStatic(boolean a){
        isStatic = a;
    }
    public void setFinal(boolean a){
        isFinal = a;
    }
    public String getType(){
        return type;
    }
    
    public String getName() {
        return name;
    }

    public String getAccessibility() {
        return accessibility;
    }

    public boolean isIsStatic() {
        return isStatic;
    }

    public boolean isIsFinal() {
        return isFinal;
    }
    
    public saveVariables(String name , String accessibility, boolean isStatic, boolean isFinal, String type){
        this.name = name;
        this.type = type;
        this.accessibility = accessibility;
        this.isFinal = isFinal;
        this.isStatic = isStatic;
    }
}
