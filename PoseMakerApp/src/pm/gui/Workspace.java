package pm.gui;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.lang.model.element.VariableElement;
import pm.controller.Connection;
import pm.controller.saveArguments;
import pm.controller.saveMethod;
import pm.controller.saveVariables;

import pm.data.DataManager;
import pm.data.VBoxPrime;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;


/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    DataManager data;
    
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    // THIS IS THE SCROLL PANE
    ScrollPane scrollPane;
    
    // STACK PANE
    Pane stackPane;
    
    public ScrollPane getScroll(){
        return scrollPane;
    }
    
    // HBOX
    VBox vBox;
    
    // BORDERPANE
    BorderPane main;
    
    // FIRST HORIZONTAL
    HBox hBox1;
    HBox hBox2;
    HBox hBox3;
    HBox hBox4;
    HBox hBox5;
    HBox tableLabel1;
    HBox tableLabel2;
    
    // TEXT FIELDS
    TextField text1;
    TextField text2;

    // Tables
    TableView table1;
    TableView table2;
    
    // COMBO BOX
    ComboBox combo1;
    public ComboBox getCombo(){
        return combo1;
    }
    private int place = -1;

    public VBox getVBox(){
        return vBox;
    }
    
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        // Init main
        main = new BorderPane();
        
        data = (DataManager) app.getDataComponent();
        
        initScrollPane(initApp);
        initStackPane(initApp);
        initVBox(initApp);
        initHBox1(initApp);
        initHBox2(initApp);
        initHBox3(initApp);
        initTables();
        
        workspace = main;
        
    }
    
    public Pane getStack(){
        return stackPane;
    }
    
     public TextField getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1.setText(text1);
    }

    public TextField getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2.setText(text2);
    }
    
    public ComboBox getPare(){
        return combo1;
    }
    
    
    private void initHBox1(AppTemplate app){
        hBox1 = new HBox(100);
        Label className = new Label("Class Name: ");
        hBox1.getChildren().add(className);
        
        text1 = new TextField();
        
        // Updates the name of the selected VBox. If name is already in use, it will not update
        text1.textProperty().addListener((e , oldValue, newValue)->{
            
            
            data.getSelected().setClassName(newValue);
            /*
            boolean name = true;
            for(int x = 0; x < data.getArray().size(); x++){
                VBoxPrime dum = (VBoxPrime) data.getArray().get(x);
                if(dum.getClassName().equals(newValue)){
                    name = false;                    
                }
                else
                {
                    data.getSelected().reset();
                }
            }*/
            data.getSelected().reset();
        });
        
        hBox1.getChildren().add(text1);
        
        vBox.getChildren().add(hBox1);
    }
    
    private void initHBox2(AppTemplate app){
        hBox2 = new HBox(115);
        Label packName = new Label("Package: ");
        hBox2.getChildren().add(packName);
        
        text2 = new TextField();
        text2.textProperty().addListener((e , oldValue, newValue)->{
            
            data.getSelected().setPackageName(newValue);
            data.getSelected().reset();
           
        });
        hBox2.getChildren().add(text2);
        
        vBox.getChildren().add(hBox2);
    }
    
    private void initHBox3(AppTemplate app){
        hBox3 = new HBox(10);
        Label parName = new Label("Parent: ");
        hBox3.getChildren().add(parName);
        
          String imagePathPlus = "file:./images/Plus.png";
        String imagePathMinus = "file:./images/Minus.png";
        Image plusImage = new Image(imagePathPlus);
        Image minusImage = new Image(imagePathMinus);
        String editIconPath = "file:./images/Edit.png";
        Image editImage = new Image(editIconPath);
        
        
        
        
        Button edit = new Button();
        edit.setGraphic(new ImageView(editImage));
        edit.setTooltip(new Tooltip("Edit"));
        
        edit.setOnAction(e->{
            Stage stage1 = new Stage();
            
            if(this.getCombo().getValue()!= null){
            //System.out.println("this");
            VBox vagin = new VBox();
            
            Scene aas = new Scene(vagin,200,200);
            stage1.setScene(aas);
            
            Button submit = new Button("Okay");
            Button close = new Button("Close");
            vagin.getChildren().add(close);
            vagin.getChildren().add(submit);
            
            TextField parField = new TextField((String)this.getCombo().getValue());
            vagin.getChildren().add(parField);
            
            submit.setOnAction(x->{
                if(data.getSelected().getParent1().equalsIgnoreCase("dum") || data.getSelected().getParent1().equalsIgnoreCase("")){
                    data.getSelected().setParent("");
                }
                String lastPar = (String)this.getCombo().getValue();
                String parentNam = parField.getText();
                //this.getCombo().getItems().add(parentNam);
                int placer = 0;
                for(int n = 0; n < this.getCombo().getItems().size();n++){
                    if(this.getCombo().getItems().get(n).equals(lastPar)){
                        placer = n;
                    }
                }
                System.out.println(placer);
                this.getCombo().setValue(parentNam);
                this.getCombo().getItems().set(placer, parentNam);
                String a = "";
                for(int m = 0; m< this.getCombo().getItems().size();m++){
                    a = a + this.getCombo().getItems().get(m) + ",";
                }
                System.out.println(a);
                data.getSelected().setParent(a);
                //System.out.println(data.getSelected().getParent1());
                stage1.close();
            });
            stage1.show();
            close.setOnAction(z->{
                stage1.close();
            });
            }
            
        });
        
        
        Button plus = new Button();
        plus.setGraphic(new ImageView(plusImage));
        plus.setTooltip(new Tooltip("Plus"));
        
        
        plus.setOnAction(e->{
            Stage stage1 = new Stage();
            
            if(data.getSelected()!= null){
            //System.out.println("this");
            VBox vagin = new VBox();
            
            Scene aas = new Scene(vagin,200,200);
            stage1.setScene(aas);
            
            Button submit = new Button("Okay");
            Button close = new Button("Close");
            vagin.getChildren().add(close);
            vagin.getChildren().add(submit);
            
            TextField parField = new TextField("Parent Name");
            vagin.getChildren().add(parField);
            
            submit.setOnAction(x->{
                if(data.getSelected().getParent1().equalsIgnoreCase("dum")){
                    data.getSelected().setParent("");
                }
                String parentNam = parField.getText();
                this.getCombo().getItems().add(parentNam);
                this.getCombo().setValue(parentNam);
                String a = data.getSelected().getParent1();
                a = a+ parentNam + ",";
                data.getSelected().setParent(a);
                //System.out.println(data.getSelected().getParent1());
                newParent(parentNam);
                stage1.close();
            });
            stage1.show();
            close.setOnAction(z->{
                stage1.close();
            });
            }
            
        });
        
        Button minus = new Button();
        minus.setGraphic(new ImageView(minusImage));
        minus.setTooltip(new Tooltip("Minus"));
        
        minus.setOnAction(e->{
            if(this.getCombo().getValue()!= null){
                System.out.println(data.getSelected().getParent1());
                String str = data.getSelected().getParent1();
                String deleteThis = (String)this.getCombo().getValue();
                String[] parents = str.split(",");
                String[] newPar = new String[100];
                String newParent = "";
                ArrayList<String> omg = new ArrayList<String>();
                for(int c = 0; c < parents.length; c++){
                    omg.add(parents[c]);
                }
                omg.remove(deleteThis);
                for(int q = 0; q < omg.size(); q++){
                    newPar[q] = omg.get(q);
                    newParent = newParent + omg.get(q) + ",";
                    System.out.println(newPar[q]);
                }
                
                this.getCombo().getItems().clear();
                //System.out.println(newPar.length);
                for(int x = 0; x < omg.size(); x++){
                
                this.getCombo().getItems().add(newPar[x]);
                this.getCombo().setValue(newPar[x]);
                
                         
            }
                data.getSelected().setParent(newParent);
                //System.out.println(data.getSelected().getParent1());
            }
        });
        
        combo1 = new ComboBox();
        hBox3.getChildren().addAll(plus,minus,edit);
        hBox3.getChildren().add(combo1);
        
        vBox.getChildren().add(hBox3);
    }
    
    private void initScrollPane(AppTemplate app){
        scrollPane = new ScrollPane();        
        scrollPane.setPrefSize(200, 200);        
        main.setCenter(scrollPane);      
        
    }
    
    private void initStackPane(AppTemplate app){
        stackPane = new Pane();
        stackPane.setMaxWidth(500000);
        stackPane.setMaxHeight(500000);
        stackPane.setMinWidth(2000);
        stackPane.setMinHeight(2000);
        stackPane.setStyle("-fx-background-color: #B9F6CA");
        //stackPane.minHeight(10000);
        //stackPane.minWidth(10000);
        
        //Rectangle test = new Rectangle(2000,2000);
        //test.setFill(Color.BEIGE);
        //stackPane.getChildren().add(test);
        
        scrollPane.setContent(stackPane);
        //scrollPane.fitToHeightProperty();
        //scrollPane.fitToWidthProperty();
        //scrollPane.setHvalue(10);
        //scrollPane.setVvalue(10);
        scrollPane.setStyle("-fx-background-color: #B9F6CA");
        scrollPane.setStyle("-fx-fill-color: #B9F6CA");
        
        
    }
    
    private void initVBox(AppTemplate app){
        vBox = new VBox(15);
        
        vBox.minHeight(500);
        vBox.minWidth(500);
                
        main.setRight(vBox);   
    }
    
    private void initTables(){
        
        table1 = new TableView();
        table2 = new TableView();
        ArrayList<VBoxPrime> VBoxes = data.getArray();
        //table1.setItems((ObservableList)VBoxes);
        //TableColumn<VBoxPrime,String> firstNameCol = new TableColumn<VBoxPrime,String>("First Name");
        //firstNameCol.setCellValueFactory(new PropertyValueFactory("firstName"));
        TableColumn nameCol = new TableColumn("Name");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn typeCol = new TableColumn("Type");
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        TableColumn staticCol = new TableColumn("Static");
        staticCol.setCellValueFactory(new PropertyValueFactory<>("isStatic"));
        TableColumn finalCol = new TableColumn("Final");
        finalCol.setCellValueFactory(new PropertyValueFactory<>("isFinal"));
        TableColumn accessCol = new TableColumn("Access");
        accessCol.setCellValueFactory(new PropertyValueFactory<>("accessibility"));
        
        
        
        table1.setEditable(true);
        table1.getColumns().addAll(nameCol , typeCol, staticCol,finalCol, accessCol);
        table1.setMinWidth(500);
        table2.setEditable(false);
        TableColumn nameCol1 = new TableColumn("Name");
        nameCol1.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn retCol = new TableColumn("Return");
        retCol.setCellValueFactory(new PropertyValueFactory<>("returnType"));
        TableColumn statCol = new TableColumn("Static");
        
        TableColumn absCol = new TableColumn("Abstract");
        //absCol.setCellValueFactory(new PropertyValueFactory<>("isAbstract"));
        absCol.setCellValueFactory(new PropertyValueFactory<saveMethod, Boolean>("isAbstract"));
        
        TableColumn finCol = new TableColumn("Final");
        finCol.setCellValueFactory(new PropertyValueFactory<>("isFinal"));
        
        TableColumn accCol = new TableColumn("Access");
        accCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        
        table2.getColumns().addAll(nameCol1,retCol,statCol,finCol,absCol,accCol);
        table2.setMinWidth(500);
        table2.setEditable(true);
        
        
        //table2.setItems((ObservableList)VBoxes);
        hBox4 = new HBox(10);
        hBox5 = new HBox(10);
        hBox4.getChildren().add(table1);
        hBox5.getChildren().add(table2);
        Label vari = new Label("Variables:");
        Label methi = new Label("Methods:");
        String imagePathPlus = "file:./images/Plus.png";
        String imagePathMinus = "file:./images/Minus.png";
        Image plusImage = new Image(imagePathPlus);
        Image minusImage = new Image(imagePathMinus);
        String editIconPath = "file:./images/Edit.png";
        Image editImage = new Image(editIconPath);
        
        Button edit1 = new Button();
        edit1.setGraphic(new ImageView(editImage));
        edit1.setTooltip(new Tooltip("Edit"));
        
        Button edit2 = new Button();
        edit2.setGraphic(new ImageView(editImage));
        edit2.setTooltip(new Tooltip("Edit"));
        
        Button plus1 = new Button();
        //plus1.setDisable(false);
        plus1.setGraphic(new ImageView(plusImage));
        Tooltip plusTooltip = new Tooltip("Plus");
        plus1.setTooltip(plusTooltip);
        
        plus1.setOnAction(e -> {
           if(data.getSelected()!=null){
            Stage stage = new Stage();
            VBox vagin = new VBox();
            
            Scene aas = new Scene(vagin,500,500);
            stage.setScene(aas);
            //stage.setMinWidth(100);
            //stage.setMinHeight(100);
            
            stage.setTitle("Add Variable");
            stage.setMinHeight(100);
            stage.setMinWidth(100);
            
            TextField nameF = new TextField("name");
            TextField typeF = new TextField("type");
            TextField statF = new TextField("true/false");
            TextField finalF = new TextField("true/false");
            TextField accessF = new TextField("public/private/etc");
            
            vagin.getChildren().add(new Label("Name : "));
            vagin.getChildren().add(nameF);
            vagin.getChildren().add(new Label("Type : "));
            vagin.getChildren().add(typeF);
            vagin.getChildren().add(new Label("Static : "));
            vagin.getChildren().add(statF);
            vagin.getChildren().add(new Label("Final : "));
            vagin.getChildren().add(finalF);
            vagin.getChildren().add(new Label("Access : "));
            vagin.getChildren().add(accessF);
            
            
            Button okay = new Button("Okay");
            
            okay.setOnAction(p -> {
                if(data.getSelected()!=null){
                String name = nameF.getText();
                String type = typeF.getText();
                String statz = statF.getText();
                String finz = finalF.getText();
                String accz = accessF.getText();
                boolean stt;
                boolean fzz;
                if(statz.equalsIgnoreCase("true")){
                    stt = true;
                }
                else{
                    stt = false;
                }
                if(finz.equalsIgnoreCase("true")){
                    fzz = true;
                }
                else{
                    fzz = false;
                }
                
                 
                saveVariables pee = new saveVariables(name,accz,stt,fzz,type);
                data.getSelected().getVariables().add(pee);
                data.setSelected(data.getSelected(), data.getVar(), data.getMeth(),data.getBig());
                String a = "";
           if(pee.getAccessibility().equalsIgnoreCase("public")){
               a += "+";
           }
           if(pee.getAccessibility().equalsIgnoreCase("private")){
               a += "-";
           }
           if(pee.isIsStatic()){
               a+= "$";
           }
          
             a+= pee.getName() + " : ";
            a+= pee.getType();
           
                data.getVar().getChildren().add(new Label(a));
                //data.reset();
                stage.close();
                }
            });
            
            
            Button close = new Button("Close");
            
            
            
            close.setOnAction(v ->  {
                stage.close();
            });
            
            vagin.getChildren().add(okay);
            vagin.getChildren().add(close);
            stage.show();
            }
        });
        
        Button minus1 = new Button();
        //minus1.setDisable(false);
        minus1.setGraphic(new ImageView(minusImage));
        Tooltip minusTooltip = new Tooltip("Minus");
        minus1.setTooltip(minusTooltip);
        
        
        minus1.setOnAction(e -> {
            
        TableViewSelectionModel selectionModel = table1.getSelectionModel();
        for(int x = 0;x<table1.getItems().size();x++){
            if(selectionModel.isSelected(x)){
                table1.getItems().remove(x);
                data.getSelected().getVariables().get(x);
                data.getVar().getChildren().remove(x);
            }
            }
        
        });
        
        
        
        Button plus2 = new Button();
        plus2.setOnAction(e->{
            if(data.getSelected()!=null){
            Stage stage = new Stage();
            VBox vagin1 = new VBox();
            Scene aas = new Scene(vagin1,100,100);
            stage.setScene(aas);
            stage.setMinWidth(500);
            stage.setMinHeight(500);
            
            stage.setTitle("Add Method");
            
            TextField nameF = new TextField("name");
            TextField typeF = new TextField("return type");
            TextField typeFR = new TextField("Type/Acc");
            TextField statF = new TextField("true/false");
            TextField finalF = new TextField("true/false");
            TextField absF = new TextField("true/false");
            
            
            
            ArrayList<saveArguments> qwerty = new ArrayList<saveArguments>();
                  
                Button anotherOne = new Button("Add Argument");
                    anotherOne.setOnAction(z ->{
                TextField ssh = new TextField("Type of Argument");
                TextField fck = new TextField("Name of Argument");
                vagin1.getChildren().add(ssh);
                vagin1.getChildren().add(fck);
                
            });
                    Button close = new Button("Close");
                    //Button test = new Button("AFSDF");
                    Button okay = new Button("Okay");
            
                    vagin1.getChildren().add(anotherOne);
                    vagin1.getChildren().add(okay);
                    vagin1.getChildren().add(close);
                    //vagin1.getChildren().add(test);
            
            
            
            vagin1.getChildren().add(new Label("Name : "));
            vagin1.getChildren().add(nameF);
            vagin1.getChildren().add(new Label("Type"));
            vagin1.getChildren().add(typeFR);
            vagin1.getChildren().add(new Label("Return Type : "));
            vagin1.getChildren().add(typeF);
            vagin1.getChildren().add(new Label("Static : "));
            vagin1.getChildren().add(statF);
            vagin1.getChildren().add(new Label("Final : "));
            vagin1.getChildren().add(finalF);
            vagin1.getChildren().add(new Label("Abstract : "));
            vagin1.getChildren().add(absF);
            
            
             
            
            
            
            
            
            
            
            close.setOnAction(v ->  {
                
                stage.close();
            });
            
            
            
            
            
            okay.setOnAction(v ->  {
                
                  
                    String name = nameF.getText();
                String type = typeF.getText();
                String statz = statF.getText();
                String finz = finalF.getText();
                String absz = absF.getText();
                String tyz = typeFR.getText();
                boolean stt1;
                boolean fzz1;
                boolean abz1;
                if(statz.equalsIgnoreCase("true")){
                    stt1 = true;
                }
                else{
                    stt1 = false;
                }
                if(finz.equalsIgnoreCase("true")){
                    fzz1 = true;
                }
                else{
                    fzz1 = false;
                }
                if(absz.equalsIgnoreCase("true")){
                    abz1 = true;
                }
                else{
                    abz1 = false;
                }
                
                for(int n = 15 ; n < vagin1.getChildren().size(); n+=2){
                    TextField typer = (TextField) vagin1.getChildren().get(n);
                    TextField arger = (TextField) vagin1.getChildren().get(n+1);
                    String typen = typer.getText();
                    String namen = arger.getText();
                    saveArguments real = new saveArguments(namen,typen);
                    qwerty.add(real);
                    //System.out.println(typen + namen);
                }
                //public saveMethod(String name, String type, String returnType, boolean isFinal, boolean isStatic, boolean isAbstract, ArrayList<saveArguments> a ){
    
                saveMethod saves = new saveMethod(name,tyz,type,fzz1,stt1,abz1,qwerty);
                
                data.getSelected().getMethods().add(saves);
                data.setSelected(data.getSelected(), data.getVar(), data.getMeth(),data.getBig());
                
                
                
                String a = "";
           
           
           if(saves.getType().equalsIgnoreCase("public")){
               a += "+";
           }
           if(saves.getType().equalsIgnoreCase("private")){
               a += "-";
           }
           a+= saves.getName()+"(";
           ArrayList<saveArguments> args1 = saves.getArgs();
           for(int zx = 0; zx < qwerty.size();zx++){
               a+= qwerty.get(zx).getType() + " : " + qwerty.get(zx).getName();
               if(zx+1 == qwerty.size()){
                   
               }
                else{
                   a+= " , ";
               }
           }
           a+= ") : " + saves.getReturnType();
           data.getMeth().getChildren().add(new Label(a));
                
                
                
                stage.close();
            });
            
            
            
            
              
            stage.show();
            
            }
        });
        //plus2.setDisable(false);
        plus2.setGraphic(new ImageView(plusImage));
        plus2.setTooltip(plusTooltip);
        
        Button minus2 = new Button();
        
        minus2.setOnAction(e -> {
            
        TableViewSelectionModel selectionModel = table2.getSelectionModel();
        for(int x = 0;x<table2.getItems().size();x++){
            if(selectionModel.isSelected(x)){
                table2.getItems().remove(x);
                data.getSelected().getMethods().get(x);
                data.getMeth().getChildren().remove(x);
            }
            }
        
        });
        //minus2.setDisable(false);
        minus2.setGraphic(new ImageView(minusImage));
        minus2.setTooltip(minusTooltip);
        
        edit1.setOnAction(e->{
            TableViewSelectionModel selectionModel = table1.getSelectionModel();
            place = -1;
                for(int x = 0;x<table1.getItems().size();x++){
                    if(selectionModel.isSelected(x)){
                        place = x;
                    }
                }
                
                if(place != -1){
                    //System.out.println(place);
                    Stage stage = new Stage();
            VBox vagin = new VBox();
            
            Scene aas = new Scene(vagin,500,500);
            stage.setScene(aas);
            //stage.setMinWidth(100);
            //stage.setMinHeight(100);
            
            stage.setTitle("Add Variable");
            stage.setMinHeight(100);
            stage.setMinWidth(100);
            
            saveVariables thisOne = data.getSelected().getVariables().get(place);
            
            TextField nameF = new TextField(thisOne.getName());
            TextField typeF = new TextField(thisOne.getType());
            TextField statF = new TextField(Boolean.toString(thisOne.isIsStatic()));
            TextField finalF = new TextField(Boolean.toString(thisOne.isIsFinal()));
            TextField accessF = new TextField(thisOne.getAccessibility());
            
            
            
            vagin.getChildren().add(new Label("Name : "));
            vagin.getChildren().add(nameF);
            vagin.getChildren().add(new Label("Type : "));
            vagin.getChildren().add(typeF);
            vagin.getChildren().add(new Label("Static : "));
            vagin.getChildren().add(statF);
            vagin.getChildren().add(new Label("Final : "));
            vagin.getChildren().add(finalF);
            vagin.getChildren().add(new Label("Access : "));
            vagin.getChildren().add(accessF);
            
            
            Button okay = new Button("Okay");
            
            okay.setOnAction(p -> {
                TableViewSelectionModel selectionModel1 = table1.getSelectionModel();
            place = -1;
                for(int x = 0;x<table1.getItems().size();x++){
                    if(selectionModel1.isSelected(x)){
                        place = x;
                    }
                }
                
                if(data.getSelected()!=null){
                String name = nameF.getText();
                String type = typeF.getText();
                String statz = statF.getText();
                String finz = finalF.getText();
                String accz = accessF.getText();
                boolean stt;
                boolean fzz;
                if(statz.equalsIgnoreCase("true")){
                    stt = true;
                }
                else{
                    stt = false;
                }
                if(finz.equalsIgnoreCase("true")){
                    fzz = true;
                }
                else{
                    fzz = false;
                }
                
                //System.out.println(place); 
                saveVariables pee = new saveVariables(name,accz,stt,fzz,type);
                data.getSelected().getVariables().add(place,pee);
                data.getSelected().getVariables().remove(place+1);
                data.setSelected(data.getSelected(), data.getVar(), data.getMeth(),data.getBig());
                String a = "";
           if(pee.getAccessibility().equalsIgnoreCase("public")){
               a += "+";
           }
           if(pee.getAccessibility().equalsIgnoreCase("private")){
               a += "-";
           }
           if(pee.isIsStatic()){
               a+="$";
           }
             a+= pee.getName() + " : ";
            a+= pee.getType();
           
                data.getVar().getChildren().add(place, new Label(a));
                data.getVar().getChildren().remove(place + 1);
                //data.reset();
                stage.close();
                }
            });
            
            
            Button close = new Button("Close");
            
            
            
            close.setOnAction(v ->  {
                stage.close();
            });
            
            vagin.getChildren().add(okay);
            vagin.getChildren().add(close);
            stage.show();
            
            }
                    
                    place = -1;
                
                
        });
        
        edit2.setOnAction(e->{
            TableViewSelectionModel selectionModel = table2.getSelectionModel();
            place = -1;
                for(int x = 0;x<table2.getItems().size();x++){
                    if(selectionModel.isSelected(x)){
                        place = x;
                    }
                }
                
                if(place != -1){
                    
                    Stage stage = new Stage();
            VBox vagin1 = new VBox();
            Scene aas = new Scene(vagin1,100,100);
            stage.setScene(aas);
            stage.setMinWidth(500);
            stage.setMinHeight(500);
            
            stage.setTitle("Edit Method");
            
            saveMethod thisOne = data.getSelected().getMethods().get(place);
            
            TextField nameF = new TextField(thisOne.getName());
            TextField typeF = new TextField(thisOne.getType());
            TextField typeFR = new TextField(thisOne.getReturnType());
            TextField statF = new TextField(Boolean.toString(thisOne.getStatic()));
            TextField finalF = new TextField(Boolean.toString(thisOne.getFinal()));
            TextField absF = new TextField(Boolean.toString(thisOne.getAbstract()));
            
            
            
            ArrayList<saveArguments> qwerty = new ArrayList<saveArguments>();
                  
                Button anotherOne = new Button("Add Argument");
                    anotherOne.setOnAction(z ->{
                TextField ssh = new TextField("Type of Argument");
                TextField fck = new TextField("Name of Argument");
                vagin1.getChildren().add(ssh);
                vagin1.getChildren().add(fck);
                
            });
                    
                    Button close = new Button("Close");
                    //Button test = new Button("AFSDF");
                    Button okay = new Button("Okay");
            
                    vagin1.getChildren().add(anotherOne);
                    vagin1.getChildren().add(okay);
                    vagin1.getChildren().add(close);
                    //vagin1.getChildren().add(test);
            
            
            
            vagin1.getChildren().add(new Label("Name : "));
            vagin1.getChildren().add(nameF);
            vagin1.getChildren().add(new Label("Type"));
            vagin1.getChildren().add(typeFR);
            vagin1.getChildren().add(new Label("Return Type : "));
            vagin1.getChildren().add(typeF);
            vagin1.getChildren().add(new Label("Static : "));
            vagin1.getChildren().add(statF);
            vagin1.getChildren().add(new Label("Final : "));
            vagin1.getChildren().add(finalF);
            vagin1.getChildren().add(new Label("Abstract : "));
            vagin1.getChildren().add(absF);
            
            
             
            
            
            
            
            
            
            
            close.setOnAction(v ->  {
                
                stage.close();
            });
            
            for(int n = 0 ; n < thisOne.getArgs().size(); n++){
                    TextField ssh = new TextField(thisOne.getArgs().get(n).getType());
                    TextField fck = new TextField(thisOne.getArgs().get(n).getName());
                    vagin1.getChildren().add(ssh);
                    vagin1.getChildren().add(fck);
                    //System.out.println(typen + namen);
                }
            
            
            
            okay.setOnAction(v ->  {
                
                  
                    String name = nameF.getText();
                String type = typeF.getText();
                String statz = statF.getText();
                String finz = finalF.getText();
                String absz = absF.getText();
                String tyz = typeFR.getText();
                boolean stt1;
                boolean fzz1;
                boolean abz1;
                if(statz.equalsIgnoreCase("true")){
                    stt1 = true;
                }
                else{
                    stt1 = false;
                }
                if(finz.equalsIgnoreCase("true")){
                    fzz1 = true;
                }
                else{
                    fzz1 = false;
                }
                if(absz.equalsIgnoreCase("true")){
                    abz1 = true;
                }
                else{
                    abz1 = false;
                }
                
                for(int n = 15 ; n < vagin1.getChildren().size(); n+=2){
                    TextField typer = (TextField) vagin1.getChildren().get(n);
                    TextField arger = (TextField) vagin1.getChildren().get(n+1);
                    String typen = typer.getText();
                    String namen = arger.getText();
                    saveArguments real = new saveArguments(namen,typen);
                    qwerty.add(real);
                    //System.out.println(typen + namen);
                }
                //public saveMethod(String name, String type, String returnType, boolean isFinal, boolean isStatic, boolean isAbstract, ArrayList<saveArguments> a ){
    
                saveMethod saves = new saveMethod(name,type,tyz,fzz1,stt1,abz1,qwerty);
                TableViewSelectionModel selectionModel1 = table2.getSelectionModel();
            place = -1;
                for(int x = 0;x<table2.getItems().size();x++){
                    if(selectionModel1.isSelected(x)){
                        place = x;
                    }
                }
                
                data.getSelected().getMethods().add(place, saves);
                data.getSelected().getMethods().remove(place+1);
                data.setSelected(data.getSelected(), data.getVar(), data.getMeth(),data.getBig());
                
                
                
                String a = "";
           
           
           if(saves.getType().equalsIgnoreCase("public")){
               a += "+";
           }
           if(saves.getType().equalsIgnoreCase("private")){
               a += "-";
           }
           a+= saves.getName()+"(";
           
           ArrayList<saveArguments> args1 = saves.getArgs();
           for(int zx = 0; zx < qwerty.size();zx++){
               a+= qwerty.get(zx).getType() + " : " + qwerty.get(zx).getName();
               if(zx+1 == qwerty.size()){
                   
               }
                else{
                   a+= " , ";
               }
           }
           
           a+= ") : " + saves.getReturnType();
           data.getMeth().getChildren().add(place, new Label(a));
           data.getMeth().getChildren().remove(place + 1); 
                
                
                stage.close();
                place = -1;
            });
            
            
            
            
              
            stage.show();
            place = -1;
                    
                }
        });
            
        
        tableLabel1 = new HBox(10);
        tableLabel2 = new HBox(10);
        tableLabel1.getChildren().add(vari);
        tableLabel2.getChildren().add(methi);
        tableLabel1.getChildren().add(plus1);
        tableLabel2.getChildren().add(plus2);
        tableLabel1.getChildren().add(minus1);
        tableLabel2.getChildren().add(minus2);
        tableLabel1.getChildren().add(edit1);
        tableLabel2.getChildren().add(edit2);
        
        vBox.getChildren().add(tableLabel1);
        vBox.getChildren().add(hBox4);
        vBox.getChildren().add(tableLabel2);
        vBox.getChildren().add(hBox5);
        
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        vBox.getStyleClass().add(flow);
        scrollPane.getStyleClass().add(flow);
        main.getStyleClass().add(flow1);
        
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
       
       
    }
    
    public void update(){
        VBoxPrime prime = data.getSelected();
        if(prime.getMethods() != null){
            ObservableList methAr = FXCollections.observableList(prime.getMethods());
            table2.getItems().clear();
            table2.getItems().addAll(methAr);    
            TableColumn statCol = (TableColumn) table2.getColumns().get(2);
            statCol.setCellValueFactory( new Callback<CellDataFeatures<saveMethod, CheckBox>, ObservableValue<CheckBox>>() {
             @Override
             public ObservableValue<CheckBox> call(
                     CellDataFeatures<saveMethod, CheckBox> arg0) {
                 saveMethod user = arg0.getValue();
                 CheckBox checkBox = new CheckBox();
                 checkBox.setSelected(user.getStatic());
                 checkBox.setOnAction(e ->{
                     
                    //checkBox.setSelected(checkBox.isSelected());
                    user.setStatic(checkBox.isSelected());
                    data.setSelected(data.getSelected(), data.getVar(), data.getMeth(), data.getBig());
                    data.resetFake();
                    
             });
                 return new SimpleObjectProperty<CheckBox>(checkBox);
             }
         });
            TableColumn finCol = (TableColumn) table2.getColumns().get(3);
            finCol.setCellValueFactory( new Callback<CellDataFeatures<saveMethod, CheckBox>, ObservableValue<CheckBox>>() {
             @Override
             public ObservableValue<CheckBox> call(
                     CellDataFeatures<saveMethod, CheckBox> arg0) {
                 saveMethod user = arg0.getValue();
                 CheckBox checkBox = new CheckBox();
                 checkBox.setSelected(user.getFinal());
                 checkBox.setOnAction(e ->{
                     //System.out.println("here");
                    //checkBox.setSelected(checkBox.isSelected());
                    user.setFinal(checkBox.isSelected());
                    data.setSelected(data.getSelected(), data.getVar(), data.getMeth(), data.getBig());
                    data.resetFake();
                    //data.setSelected(data.getSelected(), data.getVar(), data.getMeth(), data.getBig());
             });
                 return new SimpleObjectProperty<CheckBox>(checkBox);
             }
         });
            TableColumn absCol = (TableColumn) table2.getColumns().get(4);
            absCol.setCellValueFactory( new Callback<CellDataFeatures<saveMethod, CheckBox>, ObservableValue<CheckBox>>() {
             @Override
             public ObservableValue<CheckBox> call(
                     CellDataFeatures<saveMethod, CheckBox> arg0) {
                 saveMethod user = arg0.getValue();
                 CheckBox checkBox = new CheckBox();
                 checkBox.setSelected(user.getAbstract());
                 checkBox.setOnAction(e ->{
                     
                    //checkBox.setSelected(checkBox.isSelected());
                    user.setAbstract(checkBox.isSelected());
                    data.setSelected(data.getSelected(), data.getVar(), data.getMeth(), data.getBig());
                    data.resetFake();
                    //data.setSelected(data.getSelected(), data.getVar(), data.getMeth(), data.getBig());
             });
                 return new SimpleObjectProperty<CheckBox>(checkBox);
             }
         });
        }
        if(prime.getVariables() != null){
            ObservableList varAr = FXCollections.observableList(prime.getVariables());
            table1.getItems().clear();
            table1.getItems().addAll(varAr);
            TableColumn statCol = (TableColumn) table1.getColumns().get(2);
            statCol.setCellValueFactory( new Callback<CellDataFeatures<saveVariables, CheckBox>, ObservableValue<CheckBox>>() {
             @Override
             public ObservableValue<CheckBox> call(
                     CellDataFeatures<saveVariables, CheckBox> arg0) {
                 saveVariables user = arg0.getValue();
                 CheckBox checkBox = new CheckBox();
                 checkBox.setSelected(user.isIsStatic());
                 checkBox.setOnAction(e ->{
                     
                    //checkBox.setSelected(checkBox.isSelected());
                    user.setStatic(checkBox.isSelected());
                    data.setSelected(data.getSelected(), data.getVar(), data.getMeth(), data.getBig());
                    data.resetFake();
                    //data.setSelected(data.getSelected(), data.getVar(), data.getMeth(), data.getBig());
             });
                 return new SimpleObjectProperty<CheckBox>(checkBox);
             }
             });
            TableColumn finalCol = (TableColumn) table1.getColumns().get(3);
            finalCol.setCellValueFactory( new Callback<CellDataFeatures<saveVariables, CheckBox>, ObservableValue<CheckBox>>() {
             @Override
             public ObservableValue<CheckBox> call(
                     CellDataFeatures<saveVariables, CheckBox> arg0) {
                 saveVariables user = arg0.getValue();
                 CheckBox checkBox = new CheckBox();
                 checkBox.setSelected(user.isIsFinal());
                 
                 checkBox.setOnAction(e ->{
                    //checkBox.setSelected(checkBox.isSelected());
                    user.setFinal(checkBox.isSelected());
                    data.setSelected(data.getSelected(), data.getVar(), data.getMeth(), data.getBig());
                    data.resetFake();
                    //data.setSelected(data.getSelected(), data.getVar(), data.getMeth(), data.getBig());
             });
                 return new SimpleObjectProperty<CheckBox>(checkBox);
             }
         });
            
            
        }
        
    }
ArrayList<Connection> connects = new ArrayList<Connection>();
    public void newParent(String parentNam) {
        VBox orange = (VBox)data.getSelected().getParent();
        VBox apple = null ;
        for(int x = 0; x < this.getStack().getChildren().size(); x ++){
            if(this.getStack().getChildren().get(x) instanceof VBox){
            VBox pear = (VBox) this.getStack().getChildren().get(x);
            VBoxPrime pear1 = (VBoxPrime) pear.getChildren().get(0);
            if(pear1.getClassName().equalsIgnoreCase(parentNam)){
                apple = pear;
            }
        }
        }
        
        //drawLine(orange,apple);
        //System.out.println("did this dasfsdaf");
        Connection fif = new Connection(orange,apple,"Parent");
        connects.add(fif);
        this.getStack().getChildren().add(fif);
        
    }
    
    
    public ArrayList<Connection> getCon(){
        return connects;
    }
    public void setCon(ArrayList<Connection> a){
        connects = a;
    }
    ArrayList<Line> lines = new ArrayList<Line>();
    public ArrayList<Line> getLines(){
        return lines;
    }
    
    

}
