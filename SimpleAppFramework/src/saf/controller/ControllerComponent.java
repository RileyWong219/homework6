/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saf.controller;

import javafx.scene.control.CheckBox;

/**
 *
 * @author YungReezy
 */
public interface ControllerComponent {
    public void handleNewClassRequest();
    public void handleNewInterfaceRequest();
    public void handleDeleteClassRequest();
    public void handleSnapShotRequest();
    public void test();
    public void handleZoomIn();
    public void handleZoomOut();

    public void handleGridRequest(CheckBox a);

    public void handleSnapRequest(CheckBox a);

    public void handleSelectRequest();

    public void handleResizeRequest();
    
}
