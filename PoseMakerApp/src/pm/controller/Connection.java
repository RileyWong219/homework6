/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.controller;

import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import pm.data.VBoxPrime;

/**
 *
 * @author YungReezy
 */
public class Connection extends Line {
    VBox firstCon;
    VBox secCon;

    public VBox getFirstCon() {
        return firstCon;
    }

    public VBox getSecCon() {
        return secCon;
    }

    public String getRelation() {
        return relation;
    }
    String relation;
    
    public Connection(VBox a,VBox b, String rel){
        firstCon=a;
        secCon = b;
        relation = rel;
        if(a == null){
            System.out.println("we here");
        }
        if(b == null){
            System.out.println("we second line");
        }
        
        if(a!= null && b!= null){
            
            double x1 = a.getTranslateX();
            double y1 = a.getTranslateY();
            double x2 = b.getTranslateX();
            double y2 = b.getTranslateY();
            
            
            this.setStartX(x1);
            this.setStartY(y1);
            this.setEndX(x2);
            this.setEndY(y2);
                 
        }
    }
    public void follow(){
        setStartX(firstCon.getTranslateX());
            setStartY(firstCon.getTranslateY());
            setEndX(secCon.getTranslateX());
           setEndY(secCon.getTranslateY());
    }
    
    
}
