package pm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;

import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.controller.saveArguments;
import pm.controller.saveMethod;
import pm.controller.saveVariables;
import pm.data.DataManager;
import pm.data.VBoxPrime;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;


/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
    
        DataManager dataManager = (DataManager)data;
// NOW BUILD THE JSON OBJCTS TO SAVE
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	ArrayList<VBoxPrime> boxes = dataManager.getArray();
        
        for(VBoxPrime v : boxes){
            double x1 = v.getXCord();
            //System.out.println(x1);
	    double y1 = v.getYCord();
            JsonArrayBuilder meth = Json.createArrayBuilder();
            JsonArrayBuilder var = Json.createArrayBuilder();
            
            // class, package / parent/ x y / methds/ variables /
            String clas1 = v.getClassName();
            String pack = v.getPackageName();
            String parent = v.getParent1();
            String type = v.getType();
            ArrayList<saveMethod> methArray = v.getMethods();
            ArrayList<saveVariables> varArray = v.getVariables();
            
            if(v.getMethods()!= null){
            for(saveMethod methods : v.getMethods()){
                String name = methods.getName();
                String type1 = methods.getType();
                String returnT = methods.getReturnType();
                boolean fin1 = methods.getFinal();
                boolean abs1 = methods.getAbstract();
                boolean stat1 = methods.getFinal();
                JsonArrayBuilder argObjs = Json.createArrayBuilder();
                
                
                if(methods.getArgs()!= null){
                for(saveArguments args : methods.getArgs()){
                    
                    String name1 = args.getName();
                    String type2 = args.getType();
                    JsonObject argObj = Json.createObjectBuilder()
                            .add("Name", name1)
                            .add("Type", type2)
                            .build();
                    argObjs.add(argObj);
                }
                }
                JsonObject methObj = Json.createObjectBuilder()
                        .add("Name", name)
                        .add("Type", type1)
                        .add("Return Type", returnT)
                        .add("Final", fin1)
                        .add("Abstract",abs1)
                        .add("Static",stat1)
                        .add("Arguments", argObjs)
                        .build();
                meth.add(methObj);
                
            }
            }
           
            if(v.getVariables()!= null){
            for(saveVariables vars : v.getVariables()){
                
                String name = vars.getName();
                String acc = vars.getAccessibility();
                String type1 = vars.getType();
                boolean isFin = vars.isIsFinal();
                boolean isStat = vars.isIsStatic();
                JsonObject varObj = Json.createObjectBuilder()
                        .add("Name", name)
                        .add("Accessibility", acc )
                        .add("Type", type1)
                        .add("IsFinal", isFin)
                        .add("IsStatic", isStat)
                        .build();
                var.add(varObj);
                
            }
            }
            JsonObject primeProps = Json.createObjectBuilder()
                    .add("x", x1)
                    .add("y", y1)
                    .add("Class", clas1)
                    .add("Package", pack)
                    .add("Parent",parent)
                    .add("Type",type)
                    .add("Variables", var)
                    .add("Methods", meth)          
                    .build();
            //System.out.println(x1);
            
            arrayBuilder.add(primeProps);
            
        
        }
// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("Classes ", arrayBuilder.build())
                .build(); 
        
        
// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
        
    }
      
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
            // CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset1();
        
        // LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
        JsonArray class1 = json.getJsonArray("Classes ");
        
        
        for(int x = 0; x < class1.size();x++){
            JsonObject classes = class1.getJsonObject(x);
            String clas1 = classes.getString("Class");
            String pack1 = classes.getString("Package");
            String par1 = classes.getString("Parent");
            String type1 = classes.getString("Type");
            Double xCord = Double.parseDouble(classes.get("x").toString());
            Double yCord = Double.parseDouble(classes.get("y").toString());
            JsonArray varAr = classes.getJsonArray("Variables");
            JsonArray methAr = classes.getJsonArray("Methods");
            VBoxPrime addMe = new VBoxPrime("Class",clas1,dataManager);
            addMe.setTranslateX(xCord);
            addMe.setTranslateY(yCord);
            
            if(varAr != null){                                                  
                for(int n = 0; n < varAr.size();n++){
                    JsonObject var1 = varAr.getJsonObject(n);
                    String nam1 = var1.getString("Name");
                    String acc1 = var1.getString("Accessibility");
                    String tye1 = var1.getString("Type");
                    Boolean isfin = var1.getBoolean("IsFinal");
                    Boolean isstat = var1.getBoolean("IsStatic");
                    addMe.getVariables().add(new saveVariables(nam1,acc1,isstat,isfin,tye1));
                }
            }
            if(methAr != null){
                for(int n = 0; n< methAr.size(); n++){
                    JsonObject meth1 = methAr.getJsonObject(n);
                    String nam1 = meth1.getString("Name");
                    String tye1 = meth1.getString("Type");
                    String retTye1 = meth1.getString("Return Type");
                    Boolean isfin = meth1.getBoolean("Final");
                    Boolean isstat = meth1.getBoolean("Static");
                    Boolean isabs = meth1.getBoolean("Abstract");
                    /*
                    public saveMethod(String name, String type, 
                    String returnType, boolean isFinal, 
                    boolean isStatic, boolean isAbstract,
                    ArrayList<saveArguments> a ){
     
                    */
                    ArrayList<saveArguments> temp = new ArrayList<saveArguments>();
                    
                    JsonArray argsAr = meth1.getJsonArray("Arguments");
                    
                    for (int p = 0; p < argsAr.size();p++){
                        JsonObject args11 = argsAr.getJsonObject(p);
                        String nam11 = args11.getString("Name");
                        String tye11 = args11.getString("Type");
                        temp.add(new saveArguments(nam11,tye11));
                    }
                    
                    saveMethod meme = new saveMethod(nam1,tye1,retTye1,isfin,isstat,isabs,temp);
                    addMe.getMethods().add(meme);
                    
                }
            }
            
            addMe.setPackageName(pack1);
            addMe.setParent(par1);
            addMe.setType(type1);
            //Workspace test = (Workspace) app.getWorkspaceComponent();

            //System.out.println(xCord);
            //dataManager.setSelected(addMe);
            dataManager.getArray().add(addMe);
            
        }
        dataManager.reset();
    }

    // HELqPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager) data;
        ArrayList<VBoxPrime> boxes = dataManager.getArray();
        for(int x = 0; x < boxes.size(); x++){
            VBoxPrime cur = boxes.get(x);
            String pac1 = cur.getPackageName();
            pac1 = pac1.replace('.',':');
            String[] pacNames = pac1.split(":");
            if(pacNames.length ==0){
                pacNames = new String[1];
                pacNames[0] = cur.getPackageName();
            }
            String fina = "";
            fina += filePath;
            for(String cap : pacNames){
                if(cap.equals("")){
                    cap = "default";
                }
                
                fina += "/" + cap;
                Path temp = Paths.get(fina);
                if(Files.notExists(temp)){
                    new File(fina).mkdir();
                }
                
                
            
            }
                String fileDir = fina + "/"+cur.getClassName()+".java";
                File actualFile = new File (fileDir);
                actualFile.createNewFile();
                PrintWriter writer = new PrintWriter(fileDir, "UTF-8");
                if(cur.getType().equals("Class")){
                writer.println("public "+"class" + " " +cur.getClassName() + " {");
                }
                if(cur.getType().equals("Interface")){
                writer.println("public "+ "interface" + " " +cur.getClassName() + " {");
                }
                ArrayList<saveVariables> tempa = cur.getVariables();
                for(int t = 0; t < tempa.size(); t++){
                    saveVariables var = tempa.get(t);
                    writer.println(var.getType()+ " "+var.getName()+ ";");
                }
                
                System.out.println();
                ArrayList<saveMethod> tempM= cur.getMethods();
                
                for(int v = 0 ; v < tempM.size(); v++){
                    saveMethod med = tempM.get(v); 
                    ArrayList<saveArguments> argTemp = med.getArgs();
                    writer.print(med.getType().toLowerCase()+" "+ med.getReturnType() 
                            +" " 
                            +med.getName() 
                            + "(");
                    for(int c = 0; c < argTemp.size();c++){
                        saveArguments temas = argTemp.get(c);
                        writer.print(temas.getType()+ " " + temas.getName());
                    }
                    writer.print(")");
                            
                               
                   
                    
                    writer.println("{");
                    if(med.getReturnType().equalsIgnoreCase("boolean")){
                        writer.println("return true;");
                    }
                    if(med.getReturnType().equalsIgnoreCase("string")){
                        writer.println("return \" \" ");
                    }
                    if(med.getReturnType().equals("int")){
                        writer.println("return 0;");
                    }
                    writer.println("}");
                }
                
                writer.println("}");
                
                writer.close();
            
        }
        
    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
}
